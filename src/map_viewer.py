#!/usr/bin/env python
"""
This is a demo of Rviz Tools for python which tests all of the
available functions by publishing lots of Markers in Rviz.
"""

# Python includes
import numpy
import random

# ROS includes
import roslib
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, Polygon
from tf import transformations # rotation_matrix(), concatenate_matrices()

import rviz_tools_py as rviz_tools

from sensor_msgs.msg import PointCloud2

from nio_msgs.msg import around_map


class map_viewer:
    def __init__(self,topic):

        # Initialize ros node
        rospy.init_node("map_viewer", anonymous=True)

        self.topic_toolkit = {
            "/around_map_msg": {
                "topic_type":around_map,
                "callback_function":self.around_map_viewer_callback
            },
        }


        self.markers = rviz_tools.RvizMarkers('/map', 'visualization_marker')

        self.around_map_sub = rospy.Subscriber(topic,self.topic_toolkit[topic]['topic_type'],
                                               self.topic_toolkit[topic]['callback_function'])
        # self.pointcloud_sub = rospy.Subscriber("/around_map_msg",PointCloud2,self.callback)



    def around_map_viewer_callback(self,data):
        # print data.header
        # print data.links
        # print len(data.links)
        # print type(data.links)
        # print "It works..."
        display_switch = {
            'reflane':True,
            'line':True,
            'lanemark':True,
            'obj':True
        }

        reflane_type_group = [1,11]
        reflane_type_corresponding_color_group = ['blue','grey']

        line_type_group = [2,3,4,111,1110,222]
        line_type_corresponding_color_group = ['yellow','yellow','red','orange','black','brown']

        lanemark_type_group = [5,6,16,26,36,46,56,66,76,8]
        lanemark_type_corresponding_color_group = ['white']

        obj_type_group = [7,]
        obj_type_corresponding_color_group = ['blue','grey']

        links = data.links
        for link in links:
            # reflane[] reflanes
            # lanemark[] lanemarks
            # object[] objs
            # line[] lines
            if display_switch['reflane']:
                reflanes = link.reflanes
                for reflane in reflanes:
                    index_of_reflane_type = reflane_type_group.index(reflane.type)
                    points = reflane.pts
                    path = []
                    for point in points:
                        path.append(Point(point.x,point.y,0))

                    width = 0.2
                    self.markers.publishPath(path, reflane_type_corresponding_color_group[index_of_reflane_type], width, None)

            if display_switch['line']:
                lines = link.lines
                for line in lines:
                    # print line.type
                    index_of_line_type = line_type_group.index(line.type)
                    # Path:
                    # Publish a path using a list of ROS Point Msgs
                    points = line.pts
                    path = []
                    for point in points:
                        path.append(Point(point.x,point.y,0))

                    width = 0.2
                    self.markers.publishPath(path, line_type_corresponding_color_group[index_of_line_type], width, None)

            if display_switch['lanemark']:
                lanemarks = link.lanemarks
                for lanemark in lanemarks:
                    index_of_lanemark_type = lanemark_type_group.index(lanemark.type)
                    points = lanemark.pts
                    path = []
                    for point in points:
                        path.append(Point(point.x,point.y,0))

                    width = 0.2
                    self.markers.publishPath(path, lanemark_type_corresponding_color_group[0], width, None)

            if display_switch['obj']:
                objs = link.objs
                for obj in objs:
                    # print obj
                    points = obj.pts
                    height = obj.height
                    point = obj.pos
                    # Publish a cylinder using a ROS Pose
                    P = Pose(Point(point.x,point.y,0),Quaternion(0,0,0,1))
                    self.markers.publishCylinder(P, 'random', height, 1, None) # pose, color, height, radius, lifetime


    # Define exit handler
    def cleanup_node(self):
        print "Shutting down node"
        self.markers.deleteAllMarkers()


if __name__ == "__main__":


    # Initialize the around map viewer class
    mv = map_viewer('/around_map_msg')

    try:
        rospy.spin()
    except KeyboardInterrupt:
        mv.cleanup_node()
    # cv2.destroyAllWindows()
